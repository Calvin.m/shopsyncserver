package models

import io.ktor.auth.*

data class UserPrincipal(val email: String, val password: String) : Principal
