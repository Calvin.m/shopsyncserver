package models

import kotlinx.serialization.Serializable

@Serializable
data class NewShoppingListItem(
    val name: String,
    val quantity: Int = 0,
    val unit: String = "",
    val listId: Int
)
