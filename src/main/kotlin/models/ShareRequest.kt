package models

import kotlinx.serialization.Serializable

@Serializable
data class ShareRequest(val listId: Int, val userEmail: String)
