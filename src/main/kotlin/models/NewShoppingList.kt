package models

import kotlinx.serialization.Serializable

@Serializable
data class NewShoppingList(
    val name: String,
    val userId: Int
)

