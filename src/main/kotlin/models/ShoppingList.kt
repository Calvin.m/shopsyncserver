package models

import kotlinx.serialization.Serializable

//@Serializable
//data class ShoppingList(
//    val id: Int,
//    val name: String,
//    val userId: Int
//)

@Serializable
data class ShoppingList(
    val id: Int,
    val name: String,
    val userId: Int,
    @Transient
    val items: List<ShoppingListItem> = emptyList()
)