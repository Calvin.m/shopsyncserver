package models

import kotlinx.serialization.Serializable

@Serializable
data class ShoppingListItem(
    val id: Int,
    val name: String,
    val quantity: Int,
    val unit: String,
    val listId: Int
)
