package models

import kotlinx.serialization.Serializable

@Serializable
data class User(
    val id: Int,
    val email: String,
    val isAuthenticated: Boolean,
    val password: String,
    val firstName: String,
    val lastName: String
)
