import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTDecodeException
import com.auth0.jwt.exceptions.JWTVerificationException
import com.auth0.jwt.interfaces.DecodedJWT
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.serialization.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.jsonPrimitive
import models.*
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*
import kotlin.math.sign

object Users : IntIdTable("users") {
    val firstName = varchar("first_name", 255)
    val lastName = varchar("last_name", 255)
    val email = varchar("email", 255).uniqueIndex()
    val password = varchar("password", 255)
}

object ShoppingLists : IntIdTable("shopping_lists") {
    val name = varchar("name", 255)
    val userId = reference("user_id", Users)
}

object ShoppingListItems : IntIdTable("shopping_list_items") {
    val name = varchar("name", 255)
    val quantity = integer("quantity")
    val unit = varchar("unit", 255)
    val listId = reference("list_id", ShoppingLists)
}

object SharedLists : IntIdTable("shared_lists") {
    val userId = reference("user_id", Users)
    val listId = reference("list_id", ShoppingLists)
}

suspend fun getShoppingListsByUserId(userId: Int): List<ShoppingList> = withContext(Dispatchers.IO) {
    transaction {
        (ShoppingLists innerJoin Users leftJoin SharedLists)
            .select {
                (ShoppingLists.userId eq userId) or
                        ((SharedLists.userId eq userId) and (Users.id eq ShoppingLists.userId))
            }
            .map {
                ShoppingList(
                    id = it[ShoppingLists.id].value,
                    name = it[ShoppingLists.name],
                    userId = it[ShoppingLists.userId].value
                )
            }
    }
}

suspend fun getShoppingListById(listId: Int): ShoppingList? = withContext(Dispatchers.IO) {
    transaction {
        ShoppingLists.select { ShoppingLists.id eq listId }.singleOrNull()?.let {
            ShoppingList(
                id = it[ShoppingLists.id].value,
                name = it[ShoppingLists.name],
                userId = it[ShoppingLists.userId].value
            )
        }
    }
}

suspend fun shareListWithUser(listId: Int, userEmail: String): Int = withContext(Dispatchers.IO) {
    transaction {
        val userResult = Users.select { Users.email eq userEmail }.singleOrNull()
        if (userResult != null) {
            val sharedUserId = userResult[Users.id].value
            SharedLists.insertAndGetId {
                it[userId] = sharedUserId
                it[this.listId] = listId
            }.value
        } else {
            -1
        }
    }
}

suspend fun getShoppingListItemsByUserId(userId: Int): List<ShoppingListItem> = withContext(Dispatchers.IO) {
    transaction {
        (ShoppingListItems innerJoin ShoppingLists)
            .select { ShoppingLists.userId eq userId }
            .map {
                ShoppingListItem(
                    id = it[ShoppingListItems.id].value,
                    name = it[ShoppingListItems.name],
                    quantity = it[ShoppingListItems.quantity],
                    unit = it[ShoppingListItems.unit],
                    listId = it[ShoppingListItems.listId].value
                )
            }
    }
}

suspend fun getShoppingListItemsByListId(listId: Int): List<ShoppingListItem> = withContext(Dispatchers.IO) {
    transaction {
        ShoppingListItems.select { ShoppingListItems.listId eq listId }
            .map {
                ShoppingListItem(
                    id = it[ShoppingListItems.id].value,
                    name = it[ShoppingListItems.name],
                    quantity = it[ShoppingListItems.quantity],
                    unit = it[ShoppingListItems.unit],
                    listId = it[ShoppingListItems.listId].value
                )
            }
    }
}


fun main() {
    val jwtSecret = "secret_test_key"

    val url = "jdbc:postgresql://localhost:5432/shopsync"
    val user = "postgres"
    val password = "20thStreet"
//    val databaseUrl = "jdbc:postgresql://localhost:5432/shopsync?user=postgres&password=20thStreet"

    Database.connect(url, driver = "org.postgresql.Driver", user = user, password = password)

    transaction {
        // Add SharedLists to the SchemaUtils.create call
        SchemaUtils.create(Users, ShoppingLists, ShoppingListItems, SharedLists)
    }

    val server = embeddedServer(Netty, port = 8080) {
        install(ContentNegotiation) {
            json(Json {
                isLenient = true;
                ignoreUnknownKeys = true;
                allowSpecialFloatingPointValues = true;
                useArrayPolymorphism = true
            })
        }

        // Install the Authentication feature
        install(Authentication) {
            jwt("jwtAuth") {
                realm = "ShopSync"
//                verifier(JwtVerifier(jwtSecret, "HS256")) //old - doesn't work
                verifier(JWT.require(Algorithm.HMAC256(jwtSecret)).build())
                validate { credential ->

                    val email = credential.payload.getClaim("email").asString()
                    val password = credential.payload.getClaim("password").asString()

                    var foundUser: User? = null
                    transaction {
                        val userResult =
                            Users.select { Users.email.eq(email) and Users.password.eq(password) }.singleOrNull()
                        if (userResult != null) {
                            foundUser = User(
                                id = userResult[Users.id].value,
                                email = userResult[Users.email],
                                isAuthenticated = true,
                                password = userResult[Users.password],
                                firstName = userResult[Users.firstName],
                                lastName = userResult[Users.lastName]
                            )
                        }
                    }

                    if (foundUser != null) {
                        UserPrincipal(foundUser!!.email, foundUser!!.password)
                    } else {
                        null
                    }
                }
            }
        }

        @Serializable
        data class LoginRequest(val email: String, val password: String)

        @Serializable
        data class SignupRequest(val firstName: String, val lastName: String, val email: String, val password: String)
//        data class SignupRequest(val email: String?, val password: String?)

        @Serializable
        data class LogoutData(val token: String)

        @Serializable
        data class LoginData(
            val token: String
        )

        @Serializable
        data class ApiResponse(
            val status: String,
            val message: String,
            val data: LoginData
        )
        @Serializable
        data class ApiLogoutResponse(
            val status: String,
            val message: String,
            val data: LogoutData
        )

        routing {
            get("/api") {
                call.respondText("Welcome to the ShopSync API!")
            }

            post("/api/signup") {

                val signupRequest = call.receive<SignupRequest>()
                println("++++++++++++++++++")
                println("$signupRequest")
                println("++++++++++++++++++")
                val email = signupRequest.email
                val password = signupRequest.password
                val firstName = signupRequest.firstName
                val lastName = signupRequest.lastName

                if (email == null || password == null) {
                    call.respond(HttpStatusCode.BadRequest, "Email and password must be provided")
                    return@post
                }

                var existingUser: User? = null
                transaction {
                    val userResult =
                        Users.select { Users.email.eq(email) and Users.password.eq(password) }.singleOrNull()
                    if (userResult != null) {
                        existingUser = User(
                            id = userResult[Users.id].value,
                            email = userResult[Users.email],
                            isAuthenticated = true,
                            password = userResult[Users.password],
                            firstName = userResult[Users.firstName],
                            lastName = userResult[Users.lastName]
                        )
                    }
                }

                if (existingUser != null) {
                    call.respond(HttpStatusCode.Conflict, "A user with this email already exists")
                } else {
                    // Create a new user
                    var newUser: User? = null
                    transaction {
                        val newUserId = Users.insertAndGetId {
                            it[Users.email] = email
                            it[Users.password] = password
                            it[Users.firstName] = firstName
                            it[Users.lastName] = lastName
                        }
                        newUser =
                            User(
                                id = newUserId.value,
                                email = email, isAuthenticated = false,
                                password = password,
                                firstName = firstName,
                                lastName = lastName
                            )
                    }

                    // Create a JWT token
                    val token = JWT.create()
                        .withIssuer("ShopSync")
                        .withClaim("email", newUser?.email)
                        .withClaim("password", newUser?.password)
                        .sign(Algorithm.HMAC256(jwtSecret))

                    call.respond(
                        HttpStatusCode.Created,
                        ApiResponse("success", "User created successfully", LoginData(token))
                    )
                }
            }

            post("/api/login") {
                val loginRequest = call.receive<LoginRequest>()
                val email = loginRequest.email
                val password = loginRequest.password
                if (email == null || password == null) {
                    call.respond(HttpStatusCode.BadRequest, "Email and password must be provided")
                    return@post
                }

                var foundUser: User? = null
                transaction {
                    val userResult =
                        Users.select { Users.email.eq(email) and Users.password.eq(password) }.singleOrNull()
                    if (userResult != null) {
                        foundUser = User(
                            id = userResult[Users.id].value,
                            email = userResult[Users.email],
                            isAuthenticated = true,
                            password = userResult[Users.password],
                            firstName = userResult[Users.firstName],
                            lastName = userResult[Users.lastName]
                        )
                    }
                }

                if (foundUser != null) {
                    // Create a JWT token
                    val token = JWT.create()
                        .withIssuer("ShopSync")
                        .withClaim("email", foundUser?.email)
                        .withClaim("password", foundUser?.password)
                        .sign(Algorithm.HMAC256(jwtSecret))

                    val decodedToken: DecodedJWT = JWT.decode(token)
                    val expirationTime = decodedToken.expiresAt?.time // in milliseconds
                    if (expirationTime != null) {
                        println("Token will expire at: ${Date(expirationTime)}")
                    } else {
                        println("Token does not have an expiration time")
                    }

                    call.respond(HttpStatusCode.OK, ApiResponse("success", "Login successful", LoginData(token)))
                } else {
                    call.respond(HttpStatusCode.Unauthorized, "Invalid email or password")
                }
            }

            post("/api/logout") {
                try {
                    val newToken = JWT.create()
                        .withIssuer("ShopSync")
                        .withClaim("jawn", "jawn")
                        .withExpiresAt(Date(System.currentTimeMillis() + 1000)) // 1 second expiration time
                        .sign(Algorithm.HMAC256(jwtSecret))

                    call.respond(HttpStatusCode.OK, ApiLogoutResponse("success", "Logout successful", LogoutData(newToken)))
                } catch (e: JWTDecodeException) {
                    call.respond(HttpStatusCode.BadRequest, "Invalid token")
                } catch (e: Exception) {
                    call.respond(HttpStatusCode.InternalServerError, "An error occurred while processing the request")
                }
            }

            authenticate("jwtAuth") {

                //contains GET and GET/email
                route("/api/users") {
                    get {
                        val users = transaction {
                            Users.selectAll().map {
                                User(
                                    id = it[Users.id].value,
                                    email = it[Users.email],
                                    isAuthenticated = true,
                                    password = it[Users.password],
                                    firstName = it[Users.firstName],
                                    lastName = it[Users.lastName]
                                )
                            }
                        }
                        call.respond(users)
                    }

                    get("/{email}") {
                        val email = call.parameters["email"]
                        if (email != null) {
                            var foundUser: User? = null
                            transaction {
                                val userResult = Users.select { Users.email.eq(email) }.singleOrNull()
                                if (userResult != null) {
                                    foundUser = User(
                                        id = userResult[Users.id].value,
                                        email = userResult[Users.email],
                                        isAuthenticated = true,
                                        password = userResult[Users.password],
                                        firstName = userResult[Users.firstName],
                                        lastName = userResult[Users.lastName]
                                    )
                                }
                            }

                            if (foundUser != null) {
                                call.respond(mapOf("email" to foundUser?.email))
                            } else {
                                call.respond(HttpStatusCode.NotFound, "User not found")
                            }
                        } else {
                            call.respond(HttpStatusCode.BadRequest, "Invalid user ID")
                        }
                    }
                }

                // contains GET, POST, GET/id, POST/id, PUT/id, DELETE/id
                route("/api/shoppingLists") {

                    // GET shopping_lists
                    get {
                        val userEmail = call.authentication.principal<UserPrincipal>()?.email
                        if (userEmail != null) {
                            val userId = transaction {
                                Users.select { Users.email eq userEmail }.singleOrNull()?.let {
                                    it[Users.id].value
                                }
                            }

                            if (userId != null) {
                                val shoppingLists = getShoppingListsByUserId(userId)
                                call.respond(shoppingLists)
                            } else {
                                call.respond(HttpStatusCode.NotFound, "User not found")
                            }
                        } else {
                            call.respond(HttpStatusCode.Unauthorized, "Invalid token")
                        }
                    }

                    // CREATE new shopping_list
                    post {
                        val newShoppingListData = call.receive<NewShoppingList>()
                        println("Received shopping list data: $newShoppingListData")

                        try {
                            val createdShoppingList = transaction {
                                ShoppingLists.insertAndGetId {
                                    it[name] = newShoppingListData.name
                                    it[userId] = newShoppingListData.userId
                                }.value
                            }
                            println("Created shopping list ID: $createdShoppingList")
                            call.respond(HttpStatusCode.Created, mapOf("id" to createdShoppingList))
                        } catch (e: Exception) {
                            println("Error occurred: $e")
                            e.printStackTrace()
                            call.respond(
                                HttpStatusCode.InternalServerError,
                                "An error occurred while creating the shopping list"
                            )
                        }
                    }

                    // GET a shopping_list
                    get("/{id}") {
                        val id = call.parameters["id"]?.toIntOrNull()
                        if (id != null) {
                            withContext(Dispatchers.IO) {
                                val shoppingListResult = transaction {
                                    ShoppingLists.select { ShoppingLists.id eq id }.singleOrNull()?.let {
                                        ShoppingList(
                                            id = it[ShoppingLists.id].value,
                                            name = it[ShoppingLists.name],
                                            userId = it[ShoppingLists.userId].value
                                        )
                                    }
                                }

                                if (shoppingListResult != null) {
                                    val itemsResult = transaction {
                                        ShoppingListItems.select { ShoppingListItems.listId eq id }.map {
                                            ShoppingListItem(
                                                id = it[ShoppingListItems.id].value,
                                                name = it[ShoppingListItems.name],
                                                quantity = it[ShoppingListItems.quantity],
                                                unit = it[ShoppingListItems.unit],
                                                listId = it[ShoppingListItems.listId].value
                                            )
                                        }
                                    }
                                    call.respond(shoppingListResult.copy(items = itemsResult))
                                } else {
                                    call.respond(HttpStatusCode.NotFound, "Shopping list not found")
                                }
                            }
                        } else {
                            call.respond(HttpStatusCode.BadRequest, "Invalid shopping list ID")
                        }
                    }

                    // UPDATE a shopping_list
                    put("/{id}") {
                        val id = call.parameters["id"]?.toIntOrNull()
                        val updatedList = call.receive<ShoppingList>()

                        if (id != null) {
                            val updatedRowCount = transaction {
                                ShoppingLists.update({ ShoppingLists.id eq id }) {
                                    it[name] = updatedList.name
                                    it[userId] = updatedList.userId
                                }
                            }

                            if (updatedRowCount > 0) {
                                call.respond(HttpStatusCode.OK, "Shopping list updated successfully")
                            } else {
                                call.respond(HttpStatusCode.NotFound, "Shopping list not found")
                            }
                        } else {
                            call.respond(HttpStatusCode.BadRequest, "Invalid shopping list ID")
                        }
                    }

                    // DELETE a shopping list
                    delete("/{id}") {
                        val id = call.parameters["id"]?.toIntOrNull()

                        if (id != null) {
                            val deletedRowCount = transaction {
                                ShoppingLists.deleteWhere { ShoppingLists.id eq id }
                            }

                            if (deletedRowCount > 0) {
                                call.respond(HttpStatusCode.OK, "Shopping list deleted successfully")
                            } else {
                                call.respond(HttpStatusCode.NotFound, "Shopping list not found")
                            }
                        } else {
                            call.respond(HttpStatusCode.BadRequest, "Invalid shopping list ID")
                        }
                    }
                }

                route("/api/shoppingListItems") {
                    // CREATE a new shopping list item
                    post {
                        val newShoppingListItemData = call.receive<NewShoppingListItem>()
                        println("Received shopping list item data: $newShoppingListItemData")

                        try {
                            val createdShoppingListItemId = transaction {
                                ShoppingListItems.insertAndGetId {
                                    it[name] = newShoppingListItemData.name
                                    it[quantity] = newShoppingListItemData.quantity
                                    it[unit] = newShoppingListItemData.unit
                                    it[listId] = newShoppingListItemData.listId
                                }.value
                            }
                            println("Created shopping list item ID: $createdShoppingListItemId")
                            call.respond(HttpStatusCode.Created, mapOf("id" to createdShoppingListItemId))
                        } catch (e: Exception) {
                            println("Error occurred: $e")
                            e.printStackTrace()
                            call.respond(
                                HttpStatusCode.InternalServerError,
                                "An error occurred while creating the shopping list item"
                            )
                        }
                    }

                    // GET all shopping list items per user
                    get("/user") {
                        val userEmail = call.authentication.principal<UserPrincipal>()?.email
                        if (userEmail != null) {
                            val userId = transaction {
                                Users.select { Users.email eq userEmail }.singleOrNull()?.let {
                                    it[Users.id].value
                                }
                            }

                            if (userId != null) {
                                val shoppingListItems = getShoppingListItemsByUserId(userId)
                                call.respond(shoppingListItems)
                            } else {
                                call.respond(HttpStatusCode.NotFound, "User not found")
                            }
                        } else {
                            call.respond(HttpStatusCode.Unauthorized, "Invalid token")
                        }
                    }


                    // GET all shopping list items per shopping list
                    get("/list/{listId}") {
                        val listId = call.parameters["listId"]?.toIntOrNull()
                        if (listId != null) {
                            val shoppingListItems = getShoppingListItemsByListId(listId)
                            call.respond(shoppingListItems)
                        } else {
                            call.respond(HttpStatusCode.BadRequest, "Invalid list ID")
                        }
                    }

                    // UPDATE a shopping list item
                    put("/{id}") {
                        val id = call.parameters["id"]?.toIntOrNull()
                        val updatedItem = call.receive<ShoppingListItem>()

                        if (id != null) {
                            val updatedRowCount = transaction {
                                ShoppingListItems.update({ ShoppingListItems.id eq id }) {
                                    it[name] = updatedItem.name
                                    it[quantity] = updatedItem.quantity
                                    it[unit] = updatedItem.unit
                                    it[listId] = updatedItem.listId
                                }
                            }

                            if (updatedRowCount > 0) {
                                call.respond(HttpStatusCode.OK, "Shopping list item updated successfully")
                            } else {
                                call.respond(HttpStatusCode.NotFound, "Shopping list item not found")
                            }
                        } else {
                            call.respond(HttpStatusCode.BadRequest, "Invalid shopping list item ID")
                        }
                    }

                    // DELETE a shopping list item
                    delete("/{id}") {
                        val id = call.parameters["id"]?.toIntOrNull()

                        if (id != null) {
                            val deletedRowCount = transaction {
                                ShoppingListItems.deleteWhere { ShoppingListItems.id eq id }
                            }

                            if (deletedRowCount > 0) {
                                call.respond(HttpStatusCode.OK, "Shopping list item deleted successfully")
                            } else {
                                call.respond(HttpStatusCode.NotFound, "Shopping list item not found")
                            }
                        } else {
                            call.respond(HttpStatusCode.BadRequest, "Invalid shopping list item ID")
                        }
                    }


                }

                // Add the shareList route
                route("/api/shareList") {

                    // POST share a shopping_list
                    post {
                        val userEmail = call.authentication.principal<UserPrincipal>()?.email
                        if (userEmail != null) {
                            val userId = transaction {
                                Users.select { Users.email eq userEmail }.firstOrNull()?.get(Users.id)?.value
                            }

                            if (userId != null) {
                                val shareRequest = call.receive<ShareRequest>()

                                // Check if the user has access to the list
                                val list = getShoppingListById(shareRequest.listId)
                                if (list == null || list.userId != userId) {
                                    call.respond(HttpStatusCode.Forbidden, "You do not have access to this list")
                                    return@post
                                }

                                // Share the list with the specified user
                                val shareId = shareListWithUser(shareRequest.listId, shareRequest.userEmail)
                                if (shareId == -1) {
                                    call.respond(HttpStatusCode.BadRequest, "Failed to share the list")
                                } else {
                                    call.respond(HttpStatusCode.Created, "List shared successfully")
                                }
                            } else {
                                call.respond(HttpStatusCode.NotFound, "User not found")
                            }
                        } else {
                            call.respond(HttpStatusCode.Unauthorized, "Invalid token")
                        }
                    }
                }
            }
        }
    }
    server.start(wait = true)
}
