import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm

fun JwtVerifier(jwtSecret: String, algorithm: String) = JWT
    .require(Algorithm.HMAC256(jwtSecret))
    .withIssuer(algorithm)
    .build()
